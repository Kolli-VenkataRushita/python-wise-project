fonts=('Courier New',10,'bold')
from tkinter import *
import mysql.connector
from tkinter import messagebox


class Login:
    def __init__(self, root) -> None:
        self.root = root
        self.choice = Frame(self.root, width=500, height=300)
        self.choice.place(x=100, y=50)

        # Student button
        original_image_student = PhotoImage(file='C:/Users/kolli/OneDrive/Desktop/studentimage.png')
        width_student, height_student = 111, 112
        resized_image_student = original_image_student.subsample(int(original_image_student.width() / width_student), int(original_image_student.height() / height_student))
        stu_label = Label(self.choice, image=resized_image_student)
        stu_label.image = resized_image_student
        stu_label.place(x=40, y=20)
        self.student = Button(self.choice, text="Student", bg='white',font=fonts, fg='dodgerblue', width=10, command=self.goto_login_student)
        self.student.place(x=57, y=150) 

        # Seller button
        original_image_seller = PhotoImage(file='C:/Users/kolli/OneDrive/Desktop/seller.png')
        width_seller, height_seller = 100, 100
        resized_image_seller = original_image_seller.subsample(int(original_image_seller.width() / width_seller), int(original_image_seller.height() / height_seller))
        sell_label = Label(self.choice, image=resized_image_seller)
        sell_label.image = resized_image_seller
        sell_label.place(x=300, y=20)
        self.seller = Button(self.choice, text="Seller", bg='white',font=fonts, fg='dodgerblue', width=10, command=self.goto_login_seller)
        self.seller.place(x=315, y=150)

    def goto_login_student(self):
        self.choice.destroy()
        self.login_window=Frame(self.root, width=500, height=300,bg= 'white',borderwidth=4)
        self.login_window.place(x=100, y=50)
        self.root.title("Student Login")

        # Dummy username and password for demonstration
        correct_username = "student"
        correct_password = "12345"

        def check_login():
            entered_username = self.username_entry.get()
            entered_password = self.password_entry.get()

            db_connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="Rushita@2004",
            database="pillparcel")

            # Create a cursor object to interact with the database
            cursor = db_connection.cursor()

            try:
                # Execute a SELECT query to retrieve the user's details
                query = "SELECT * FROM student WHERE user_name = %s AND password = %s"
                cursor.execute(query, (entered_username, entered_password))

                # Fetch the result
                result = cursor.fetchone()

                if result:
                    # User exists, login successful
                    print("Login successful!")
                    self.login_window.destroy()
                    messagebox.showinfo('WELCOME', 'WELCOME USER')


                else:
                    # User not found or password incorrect
                    print("Invalid login credentials.")

            except mysql.connector.Error as err:
                print(f"Error: {err}")

            finally:
                # Close the cursor and connection
                cursor.close()
                db_connection.close()

        username_label=Label(self.login_window,text='UserName: ',font= ('Arial', 15),bg='white',fg='steel blue',width=15)
        username_label.place(x=60,y=80)
        self.username_entry=Entry(self.login_window,bg='white',fg='steel blue',font= ('Arial', 15),width=17)
        self.username_entry.place(x=200,y=80)

        password_label=Label(self.login_window,text='Password: ',font= ('Arial', 15),bg='white',fg='steel blue',width=15)
        password_label.place(x=60,y=150)
        self.password_entry=Entry(self.login_window,bg='white',fg='steel blue',font= ('Arial', 15),width=17,show='x')
        self.password_entry.place(x=200,y=150)

        login_button = Button(self.login_window, text="Login",font=('Arial', 11), command=check_login,bg='white',fg='steel blue')
        login_button.place(x=210, y=200)

        lable= Label(self.login_window,text="Not have an account..?",font=('Arial',10),fg='black',bg='white')
        lable.place(x=250,y=20)
        signup_button = Button(self.login_window, text="SignUp",font= ('Arial', 11), command=self.goto_signup_student,bg='white',fg='steel blue')
        signup_button.place(x=390, y=15)


    def goto_login_seller(self):
        print("Seller button clicked")
        self.choice.destroy()
        self.login_window=Frame(self.root, width=500, height=300,bg= 'white',borderwidth=4)
        self.login_window.place(x=100, y=50)
        self.root.title("Seller Login")

        # Dummy username and password for demonstration
        correct_username = "seller"
        correct_password = "12345"

        def check_login():
            entered_username = self.username_entry.get()
            entered_password = self.password_entry.get()

            if entered_username == correct_username and entered_password == correct_password:
                print("Login successful for seller")
                self.login_window.destroy()
                db_connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="Rushita@2004",
            database="pillparcel")

            # Create a cursor object to interact with the database
            cursor = db_connection.cursor()

            try:
                # Execute a SELECT query to retrieve the user's details
                query = "SELECT * FROM student WHERE user_name = %s AND password = %s"
                cursor.execute(query, (entered_username, entered_password))

                # Fetch the result
                result = cursor.fetchone()

                if result:
                    # User exists, login successful
                    print("Login successful!")
                    self.login_window.destroy()
                    messagebox.showinfo('WELCOME', 'WELCOME USER')


                else:
                    # User not found or password incorrect
                    print("Invalid login credentials.")

            except mysql.connector.Error as err:
                print(f"Error: {err}")

            finally:
                # Close the cursor and connection
                cursor.close()
                db_connection.close()
                # You can add your code here for what happens after successful login

                

        username_label=Label(self.login_window,text='UserName: ',font= ('Arial', 15),bg='white',fg='steel blue',width=15)
        username_label.place(x=60,y=80)
        self.username_entry=Entry(self.login_window,bg='white',fg='steel blue',font= ('Arial', 15),width=17)
        self.username_entry.place(x=200,y=80)

        password_label=Label(self.login_window,text='Password: ',font= ('Arial', 15),bg='white',fg='steel blue',width=15)
        password_label.place(x=60,y=150)
        self.password_entry=Entry(self.login_window,bg='white',fg='steel blue',font= ('Arial', 15),width=17,show='x')
        self.password_entry.place(x=200,y=150)

        login_button = Button(self.login_window, text="Login",font=('Arial', 11), command=check_login,bg='white',fg='steel blue')
        login_button.place(x=210, y=200)

        lable= Label(self.login_window,text="Not have an account..?",font=('Arial',10),fg='black',bg='white')
        lable.place(x=250,y=20)
        signup_button = Button(self.login_window, text="SignUp",font= ('Arial', 11), command=self.goto_signup_seller,bg='white',fg='steel blue')
        signup_button.place(x=390, y=15)

    def goto_signup_student(self):
        print("student not have account")
        self.login_window.destroy()
        self.choice=Frame(self.root,width=500,height=300,bg='white')
        self.choice.place(x=100,y=50)

        self.stu_name=Label(self.choice,text='Name: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.stu_name.place(x=50,y=60)
        self.name=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.name.place(x=230,y=60)

        self.stu_age=Label(self.choice,text='Age: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.stu_age.place(x=50,y=100)
        self.age=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.age.place(x=230,y=100)

        self.stu_username=Label(self.choice,text='UserName: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.stu_username.place(x=50,y=140)
        self.username=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.username.place(x=230,y=140)

        self.stu_passw=Label(self.choice,text='Set Password: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.stu_passw.place(x=50,y=180)
        self.password=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17,show='x')
        self.password.place(x=230,y=180)

        self.signup_btn=Button(self.choice,text="SignUp",bg='white',fg='steel blue',font=fonts,width=10,command=self.stu_home)
        self.signup_btn.place(x=200,y=230)

    
    def goto_signup_seller(self):
        print("seller do't have account")
        self.login_window.destroy()
        self.choice=Frame(self.root,width=500,height=300,bg='white')
        self.choice.place(x=100,y=50)

        self.sell_name=Label(self.choice,text='Name: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.sell_name.place(x=50,y=60)
        self.name=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.name.place(x=230,y=60)

        self.sell_add=Label(self.choice,text='Address: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.sell_add.place(x=50,y=100)
        self.address=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.address.place(x=230,y=100)

        self.sell_mobileno=Label(self.choice,text='MobileNo:',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.sell_mobileno.place(x=50,y=140)
        self.mobileno=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.mobileno.place(x=230,y=140)

        self.sell_username=Label(self.choice,text='UserName: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.sell_username.place(x=50,y=180)
        self.username=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17)
        self.username.place(x=230,y=180)

        self.sell_passw=Label(self.choice,text='Set Password: ',font=('Courier New',15,'bold'),bg='white',fg='steel blue',width=15)
        self.sell_passw.place(x=50,y=220)
        self.password=Entry(self.choice,bg='white',fg='steel blue',font=('Courier New',13,'bold'),width=17,show='x')
        self.password.place(x=230,y=220)

        self.signup_btn=Button(self.choice,text="SignUp",bg='white',fg='steel blue',font=fonts,width=10,command=self.sell_home)
        self.signup_btn.place(x=200,y=260)

    def stu_home(self):
        
        if self.name.get()=="" or self.age.get()=="" or self.username=="" or self.password=="":
            messagebox.showerror('error', 'error enter details correctly')
        else:
            conn = mysql.connector.connect(host="localhost",user="root",password="Rushita@2004",database="pillparcel")
            my_cursor=conn.cursor()
            self.query = f"insert into student values('{self.name.get()}',{self.age.get()},'{self.username.get()}','{self.password.get()}')"
            my_cursor.execute(self.query)
            self.choice.destroy()
            messagebox.showinfo('WELCOME', 'WELCOME USER')
            conn.commit()
            conn.close()

    def sell_home(self):
        if self.name.get()=="" or self.address.get()=="" or self.mobileno.get()=="" or self.username=="" or self.password=="":
            messagebox.showerror('error', 'error enter details correctly')
        else:
            conn = mysql.connector.connect(host="localhost",user="root",password="Rushita@2004",database="pillparcel")
            my_cursor=conn.cursor()
            self.query = f"insert into sellar values('{self.name.get()}','{self.address.get()}','{self.mobileno.get()}','{self.username.get()}','{self.password.get()}')"
            my_cursor.execute(self.query)
            self.choice.destroy()
            messagebox.showinfo('WELCOME', 'WELCOME USER')
            conn.commit()
            conn.close()
root = Tk()
root.title('PILL PARCEL')
root.geometry('700x400+450+100')
root.resizable(False, False)

login = Login(root)
root.mainloop()

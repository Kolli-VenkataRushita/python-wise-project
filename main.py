from tkinter import Tk, Frame, Label, Entry, Button, messagebox, PhotoImage

# from PIL import Image,ImageTk
fonts = ('Courier New', 15, 'bold')
user1 = 'Person1'
passw1 = '123'
user2 = 'Person2'
passw2 = '456'


class Login:
    def __init__(self, root):
        # User and Seller login
        self.root = root
        self.login_frame = Frame(self.root, width=250, height=300, bg='white')
        self.login_frame.place(x=40, y=50)
        self.login_frame2 = Frame(self.root, width=250, height=300, bg='white')
        self.login_frame2.place(x=400, y=50)

        self.user_name = Label(self.login_frame, text='Seller: ', font=fonts, bg='white', fg='steel blue', width=10)
        self.user_name.place(x=25, y=50)
        self.user_name_entry1 = Entry(self.login_frame, width=8, font=fonts, bg='white')
        self.user_name_entry1.place(x=140, y=50)
        self.user_pass = Label(self.login_frame, text='Password: ', font=fonts, bg='white', fg='steel blue', width=10)
        self.user_pass.place(x=20, y=110)
        self.user_pass_entry1 = Entry(self.login_frame, width=8, font=fonts, bg='white', show="*")
        self.user_pass_entry1.place(x=140, y=110)
        self.submit_btn = Button(self.login_frame, text='LogIn', bg='white', fg='steel blue', font=fonts,command=self.check_login_seller)

        self.submit_btn.place(x=80, y=170)
        self.user_name = Label(self.login_frame2, text='Student: ', font=fonts, bg='white', fg='steel blue', width=10)
        self.user_name.place(x=25, y=50)
        self.user_name_entry2 = Entry(self.login_frame2, width=8, font=fonts, bg='white')
        self.user_name_entry2.place(x=140, y=50)

        self.user_pass = Label(self.login_frame2, text='Password: ', font=fonts, bg='white', fg='steel blue', width=10)
        self.user_pass.place(x=20, y=110)
        self.user_pass_entry2 = Entry(self.login_frame2, width=8, font=fonts, bg='white', show="*")
        self.user_pass_entry2.place(x=140, y=110)

        self.submit_btn = Button(self.login_frame2, text='LogIn', bg='white', fg='steel blue', font=fonts,
                                 command=self.check_login_student)
        self.submit_btn.place(x=80, y=170)



    def check_login_seller(self):
        self.name = self.user_name_entry1.get()
        self.password = self.user_pass_entry1.get()
        if self.name == user1:
            if self.password == passw1:
                messagebox.showinfo('WELCOME', 'WELCOME USER')
                self.login_frame.destroy()
                dashboard = Dashboard(self.root)
            else:
                messagebox.showerror('WRONG PASSWORD', 'CHECK YOUR PASSWORD')
        else:
            messagebox.showerror('WROMG ID', 'INVALID USERNAME')

    def check_login_student(self):
        self.name = self.user_name_entry2.get()
        self.password = self.user_pass_entry2.get()
        if self.name == user2:
            if self.password == passw2:
                messagebox.showinfo('WELCOME', 'WELCOME USER')
                self.login_frame2.destroy()
                dashboard = Dashboard(self.root)
            else:
                messagebox.showerror('WRONG PASSWORD', 'CHECK YOUR PASSWORD')
        else:
            messagebox.showerror('WROMG ID', 'INVALID USERNAME')


class Dashboard:
    def _init_(self, root):
        self.root = root
        self.root.title('WELCOME TO THE DASHBOARD')
        self.d_frame = Frame(self.root, width=550, height=350, bg='blue')
        self.d_frame.place(x=0, y=0)


root = Tk()
root.title('PILL PARCEL')
root.geometry('700x400+450+100')
root.resizable(False, False)
login = Login(root)
root.mainloop()